//
//  TableViewCell.swift
//  Concurrency
//
//  Created by admin on 26/09/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

	static let Identifier = "tableViewCell"
	
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var progressLabel: UILabel!
	@IBOutlet weak var thumbnailImage: UIImageView!
	
	var cachedImageFileURL: URL!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	func downloadImage(urlString: String){
//		cachedImageFileURL = try! FileManager.default
//			.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
//			.appendingPathComponent(urlString)
		let urlSession = URLSession(configuration: URLSessionConfiguration.background(withIdentifier: String(Int.random(in: 1..<10000))), delegate: self, delegateQueue: OperationQueue.main)
		guard let url = URL(string: urlString) else { return }
		DispatchQueue.main.async {
			let task = urlSession.downloadTask(with: url)
			task.resume()
		}
		
	}
}

extension TableViewCell {
	func addObservers(){
		NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(applicationEnterToBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
	}
	
	@objc func applicationDidBecomeActive(){
		
	}
	
	@objc func applicationEnterToBackground(){
		
	}
}

extension TableViewCell : URLSessionDownloadDelegate{
	func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
		print("Download Completed")
		
//		try! FileManager.default.copyItem(at: location, to: cachedImageFileURL)
		let data = try! Data(contentsOf: location)
		
		DispatchQueue.main.async {
			self.thumbnailImage.image = UIImage(data: data)
		}
	}
	
	func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
		print("Loading (\((Float(totalBytesWritten)/Float(totalBytesExpectedToWrite)) * 100)%)")
		DispatchQueue.main.async {
			self.progressLabel.text = "Downloading (\((Float(totalBytesWritten)/Float(totalBytesExpectedToWrite)) * 100)%) done..."
		}
	}
}

extension TableViewCell: URLSessionDelegate {
	
}

