//
//  ImageHelper.swift
//  Concurrency
//
//  Created by admin on 26/09/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation


final class ImageHelper {
	
	static func getImages() -> [String] {
		return ["https://upload.wikimedia.org/wikipedia/commons/0/06/Master_of_Flémalle_-_Portrait_of_a_Fat_Man_-_Google_Art_Project_(331318).jpg",
		"https://upload.wikimedia.org/wikipedia/commons/0/04/Dyck,_Anthony_van_-_Family_Portrait.jpg",
		"https://upload.wikimedia.org/wikipedia/commons/c/ce/Petrus_Christus_-_Portrait_of_a_Young_Woman_-_Google_Art_Project.jpg",
		"https://upload.wikimedia.org/wikipedia/commons/3/36/Quentin_Matsys_-_A_Grotesque_old_woman.jpg",
		"https://upload.wikimedia.org/wikipedia/commons/c/c8/Valmy_Battle_painting.jpg"]
	}
}
