//
//  TableViewController.swift
//  Concurrency
//
//  Created by admin on 26/09/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {

	let images: [String] = ImageHelper.getImages()
	
    override func viewDidLoad() {
        super.viewDidLoad()
    }

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.Identifier) as! TableViewCell
		cell.titleLabel.text = images[indexPath.row]
		cell.downloadImage(urlString: images[indexPath.row])
		return cell
	}
	
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return ImageHelper.getImages().count
    }
}

